package com.caldo.pagination.springdatapagination.Repository;

import com.caldo.pagination.springdatapagination.Model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
