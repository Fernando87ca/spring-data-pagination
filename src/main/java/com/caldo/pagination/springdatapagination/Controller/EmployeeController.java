package com.caldo.pagination.springdatapagination.Controller;

import com.caldo.pagination.springdatapagination.Model.Employee;
import com.caldo.pagination.springdatapagination.Service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employeeList")
    public Page<Employee> getEmployeesPageable(Pageable pageable) {
        return employeeService.getEmployees(pageable);
    }

}
