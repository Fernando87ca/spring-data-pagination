package com.caldo.pagination.springdatapagination.Service;

import com.caldo.pagination.springdatapagination.Model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeeService {
    Page<Employee> getEmployees(Pageable pageable);
}
