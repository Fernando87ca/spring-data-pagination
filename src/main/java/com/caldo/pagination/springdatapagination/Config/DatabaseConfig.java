package com.caldo.pagination.springdatapagination.Config;

import com.caldo.pagination.springdatapagination.Model.Employee;
import com.caldo.pagination.springdatapagination.Repository.EmployeeRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Configuration
public class DatabaseConfig {

    @Bean
    public EmployeeRepository EmployeeRepositoryInitConfig(EmployeeRepository employeeRepository) {
        List<Employee> employees = IntStream.rangeClosed(1, 1000)
                .mapToObj(loop -> new Employee()
                        .setId(loop)
                        .setName("name" + loop)
                        .setDepartment("Department" + loop))
                .collect(Collectors.toList());

        employeeRepository.save(employees);
        return employeeRepository;
    }

}
